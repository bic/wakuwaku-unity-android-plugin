WAKU WAKU Unity Plugin for Android
==================================

This plugin allows to integrate the WAKU WAKU service into Android applications based on [Unity](http://unity3d.com/).

Usage
-----

Integration relies on a few steps:

* Get the plugin from the [Bitbucket repository](https://bitbucket.org/bic/wakuwaku-unity-android-plugin/downloads/wakuwaku-unity-android-wrapper.zip).
* Add the plugin to your project.
* Initialize the SDK within the project.
* Add at least a call request for coupons.

Get the plugin
--------------

The plugin is currently distributed as a ZIP archive downloadable from the [Bitbucket repository](https://bitbucket.org/bic/wakuwaku-unity-android-plugin/downloads/wakuwaku-unity-android-wrapper.zip).

The contents of the archive is sufficient to complete the integration: It contains the necessary assets and all its dependencies to the WAKU WAKU Android SDK and Android support library.

Add the plugin to your project
------------------------------

The plugin archive contains only files that will go under `Assets/Plugins/Android` in your project directory.

You can check the exact content by listing the archive:

    unzip -l

At time of writting, the contents is:

    Archive:  wakuwaku-unity-android-wrapper.zip
      Length     Date   Time    Name
     --------    ----   ----    ----
            0  04-22-14 12:21   Assets/
            0  04-22-14 12:21   Assets/Plugins/
            0  04-22-14 14:41   Assets/Plugins/Android/
       648327  04-22-14 14:41   Assets/Plugins/Android/android-support-v4.jar
         2049  04-21-14 17:07   Assets/Plugins/Android/AndroidManifest.xml
       159057  04-22-14 14:41   Assets/Plugins/Android/wakuwaku-sdk-latest.jar
         1171  04-22-14 12:07   Assets/Plugins/Android/WakuWakuAndroidPlugin.cs
     --------                   -------
       810604                   7 files

*Important*: Before extracting the archive, please double check that it will not overwrite any of your existing files (e.g. AndroidManifest.xml)! `unzip` does not overwrite *by default* on most distributions, but just in case, you can use `unzip -n` to make sure the tool asks you what to do in case of overwrite.

Add the plugin by simply extracting the archive from your project home directory:

    unzip -n wakuwaku-unity-android-wrapper.zip

If `Assets/Plugins/Android` does not exist, `unzip` takes care of creating the hierarchy for us.

Initialize the SDK within the project
-------------------------------------

The WAKU WAKU SDK needs be activated with your company and application IDs, as well as some options to ease integration endeavours. And at some point in the flow of the application, some coupon requests need to appear!

Activation of the WAKU WAKU SDK is best done during initialization of the application. It requires a single call:

    WakuWakuAndroidPlugin.activate(
                      "[YOUR COMPANY ID]",     // See below
                      "[YOUR APP ID]",         // See below
                      false | true,            // Production mode?
                      "landscape" | "portrait" // Desired coupon orientation
                     );

Sample call:

    WakuWakuAndroidPlugin.activate("mycompanyid", "myappid", false, "landscape");

The parameters:

- YOUR COMPANY ID (a 128-char string)
- YOUR APP ID     (a UUID string)

These IDs are provided by WAKU WAKU. Please contact the company to get them.

You can also customize two parameters.

- Production mode: `true` to get real coupons, `false` for testing
                   with fake coupons as much as you want.
- Orientation of coupons: `"landscape"` or `"portrait"`.

Note: The IDs are for tracking your account and determine what we pay you at the end of each cycle. These IDs are not aimed at security.


Add at least a call request for coupons
---------------------------------------

In the application, when the user has achieved some milestone (you choose!), please add a call to request a coupon.

    WakuWakuAndroidPlugin.showCoupon();

Please consider pausing/resuming the game around the coupon invocation. The current coupon is shown into an Android Dialog instance, which does not affect the current activity lifecycle the same way an activity does.

What it does
------------

This SDK fetches a coupon in the background, ready to get displayed when the player has achieved the target you have chosen in the game (e.g. complete a level, acquired some object, collected X gems/coins). When the achievement is completed, the SDK displays an Android Dialog that shows the coupon, and allows to send it by email, or to discard it. The email needs be entered only once.

The dialog also shows terms of use, a WAKU button that links to the service home page, and a "W" button for the upcoming Wallet application (possibly a Cancel button instead), for coupon management.


Coupon or not Coupon?
---------------------

Depending on many conditions defined by advertisers, availability, location, user demographics, and yourself, the request MAY or MAY NOT return a coupon. This call request code handles the cases as follows:

* If a coupon is available, it is shown in a dialog.
* If no coupon is available, the call is ignored and the flow returns to the caller.

Call requests are independentent, so if the user has received no coupon at some point (and for some varying reasons), she may well receive it at the next call.


Other resources
---------------

This Unity Plugin for Android is basically a wrapper for the WAKU WAKU Android SDK. You might be interested in the [SDK repository](https://bitbucket.org/freakhill/wakuwaku-android-sdk) to see, or even customize, the SDK itself. Also, we package updates to the SDK into the plugin as fast as we can, but if you need to, you can just change the SDK JAR file by any version found on the [download page](https://bitbucket.org/freakhill/wakuwaku-android-sdk/downloads).


License
-------

This SDK is distributed under the standard MIT License. Please see LICENSE.md in this folder for more information.


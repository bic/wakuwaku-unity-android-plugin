using UnityEngine;
using System.Collections;
using System;

public class WakuWakuAndroidPlugin {

  public static void activate(string companyId, string appId, Boolean productionMode, string orientation) {
    activate(companyId, appId, productionMode, orientation, false);
  }

  public static void activateAsync(string companyId, string appId, Boolean productionMode, string orientation) {
    activateAsync(companyId, appId, productionMode, orientation, false);
  }

  public static void activate(string companyId, string appId, Boolean productionMode, string orientation, Boolean okToGetLocation) {
    doActivate("activate", companyId, appId, productionMode, orientation, okToGetLocation);
  }

  public static void activateAsync(string companyId, string appId, Boolean productionMode, string orientation, Boolean okToGetLocation) {
    Debug.Log ("WAKU WAKU Asynchronous Activation");
    doActivate("activateAsync", companyId, appId, productionMode, orientation, okToGetLocation);
  }

  public static void showCoupon() {
    if (!initialized || !activated) {
      Debug.Log ("WAKU WAKU plugin not activated. Ignored coupon request.");
    } else {
      Debug.Log ("Preparing WAKU WAKU coupon");
      Wakuwaku.CallStatic<bool> ("couponDialog");
    }
  }

  // Since 1.2.0
  public static bool getCoupon(out string couponID, out Texture2D couponImage) {
    Debug.Log("Getting coupon");
    if (!initialized) { initialize (); }
    using (AndroidJavaObject coupon = Wakuwaku.CallStatic<AndroidJavaObject>("take")) {
      if (coupon != null) {
        string couponIdentifier = coupon.Call<string>("getCouponID");
        if (couponIdentifier != null) {
          byte[] drawable = coupon.Call<byte[]>("getDrawableBytes");
          if (drawable != null) {
            Texture2D couponContents = doGetCouponImage(drawable);
            coupon.Dispose(); // TODO Not enough. Need to be done in case of no drawable too.
            if (couponContents != null) {
              couponID = couponIdentifier;
              couponImage = couponContents;
              return true;
            }
          }
          Debug.Log("Could not download the coupon image. Problem with network or storage?");
        }
      }
    }
    Debug.Log("No coupon found yet.");
    couponID = null;
    couponImage = null;
    return false;
  }

  // Since 1.2.0
  public static bool sendCouponToEmail(string couponID, string emailAddress) {
    Debug.Log ("Sending coupon by email");
    if (!initialized) { initialize (); }
    if (couponID != null && emailAddress != null && emailAddress.Trim().Length > 0) {
      using (AndroidJavaObject coupon = Wakuwaku.CallStatic<AndroidJavaObject>("findCoupon", couponID)) {
        if (coupon != null) {
          Wakuwaku.CallStatic("setEmail", emailAddress.Trim());
          return coupon.Call<bool>("toEmail");
        }
      }
    }
    Debug.Log("Invalid argument: couponID=" + couponID + ", email=" + emailAddress);
    return false;
  }

  // Internal API
  //
  private static Boolean initialized = false;
  private static Boolean activated   = false;
  private static AndroidJavaClass UnityPlayer;
  private static AndroidJavaClass Wakuwaku;

  private static void initialize() {
    UnityPlayer = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
    Wakuwaku = new AndroidJavaClass ("jp.ne.wakuwaku.sdk.Wakuwaku");
    initialized = true;
    Debug.Log ("WAKU WAKU Initialized");
  }

  private static void doActivate(string activationCall, string companyId, string appId, Boolean productionMode, string orientation, Boolean okToGetLocation) {
    if (!initialized) { initialize (); }
    if (!activated) {
      AndroidJavaObject activity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
      Wakuwaku.CallStatic(activationCall, activity, companyId, appId, productionMode, orientation, okToGetLocation);
      activated = true;
      Debug.Log ("WAKU WAKU Activated");
    } else {
      Debug.Log ("WAKU WAKU already activated. Ignoring.");
    }
  }

  // Since 1.2.0
  private static Texture2D doGetCouponImage(byte[] drawable) {
    Debug.Log("Preparing image texture");
    if (drawable != null) {
      Texture2D image = new Texture2D(1, 1);
      image.LoadImage(drawable);
      return image;
    }
    return null;
  }

}

